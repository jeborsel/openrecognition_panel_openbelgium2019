### What's the status of Skill Recognition in the Labor Market? 
Panel Discussion [Open Belgium 2019](http://2019.openbelgium.be/session/whats-status-skill-recognition-labor-market)

Note:

(Start time 14h15)
Welcome all to this Panel session on Skills Recognition. My name is Bert, I will be the moderator of this session,
but I am also co-founder of Open Recognition Belgium working group and not totally objective in this debate
So in case that I as moderator, need some moderating, please do raise your voice and put me to order.

#HSLIDE

@snap[west splitscreen]
### Open Content to Enable Refugees to build Digital Skills
![Hack Your Future](assets/images/HYFpic2.jpg)
@snapend


@snap[east splitscreen]
### Open Summer of Code: The Next Generation of Open Source

![Open Summer of Code](assets/images/osocpicture2.jpg)
@snapend

Note:

For those who are following the Open Education track here at Open Belgium, you maybe saw these 2 presentations in the morning session.
They are examples of projects in which students are developing new skills.

#HSLIDE

@snap[west persona]
### Open Content to Enable Refugees to build Digital Skills
![Issam](assets/images/Issam.png)
## Meet Issam
@ul[](false)
- Issam is a Syrian refugee
- has a academic degree as Pharmacist
- had his own business in Aleppo 
- participated in HYF
@ulend
@snapend


@snap[east persona]
### Open Summer of Code: The Next Generation of Open Source
![Sophie](assets/images/Sophie.png)
## Meet Sophie
@ul[](false)
- Sophie is a Digital Design student
- she is in her final year, but feels like she isn't learning much new
- received her first job offer
- participated in oSoc 
@ulend
@snapend

Note:

We will take 2 students: Issam & Sophie on a journey from HYF & oSoc towards the labor market to frame the statements, that we will use for panel discussion and have a realistic example in mind 

#HSLIDE
### **Voice Vote** 
![HouseOfLords](assets/images/houseoflords.jpg)

Note:

We will be low on tech today, so no fancy mobile app to vote, we don't want you on your phones in this session.

#HSLIDE
### supporters say 'aye' 
It is a good thing that Education has opened up to facilitate lifelong learning and Issam & Sophie can gather their knowledge & skills anywhere.

Note:

a first statement to warm up 

#HSLIDE
### opponents say 'nay'
It is a good thing that Education has opened up to facilitate lifelong learning and Issam & Sophie can gather their knowledge & skills anywhere.

#HSLIDE
### Panel

#HSLIDE
@snap[midpoint panel]
### Panel
![Saskia Van Uffelen](assets/images/SaskiaVanUffelen.jpeg)
## Saskia Van Uffelen
@ul[](false)
- Digital Champion for Belgium
- Co-founder of BeCentral
- Promoting the development of Digital Skills in Belgium
@ulend
@snapend

#HSLIDE
@snap[midpoint panel]
### Panel
![Vincent Van Malderen](assets/images/vincentvanmalderen.png)
## Vincent Van Malderen
@ul[](false)
- Director Poolstok
- Promoting data-driven and evidence based HR
- Spreading the message of Open Badges
@ulend
@snapend

#HSLIDE
@snap[midpoint panel]
### Panel
![An De Coen](assets/images/AnDeCoen.png)
## An De Coen 
@ul[](false)
- Senior Consultant Skills and Labour market
- PhD in Applied Economics (on age & employability)
- Published a rapport on Company Perspective towards Recognition of Prior Learning
@ulend
@snapend

#HSLIDE
@snap[midpoint panel]
### Panel
![Serge Ravet](assets/images/SergeRAVET.png)
## Serge Ravet
@ul[](false)
- President of the Open Recognition Alliance
- Strong believer in the Empowerment of individuals and communities
- Core Open Badges thinkerer on Trust & Digital Identity
@ulend
@snapend

#HSLIDE
@snap[midpoint panel]
### Panel
![Alison Crabb](assets/images/AlisonCrabb.jpeg)
## ALison Crabb
@ul[](false)
- Head of Unit Skills and Qualifications, DG Employment,Social Affairs and Inclusion, European Commission
- The team behind e.g. ESCO, Europass, Skills Profile Tool,...
- Previously involved in vocational education & adult learning
@ulend
@snapend

#HSLIDE
@snap[full]
![Skills Index](assets/images/skillsindex.png)
@snapend

Note:

(8 min)
So that is the panel, now the topic.
The European Skills Index (ESI) is Cedefop’s composite indicator measuring the performance of EU skills systems
- **Skills Development** represents the training and education activities of the country and the immediate outputs of that system
- **Skills Activation** includes indicators of the transition from education to work, together with labour market activity rates for different groups of the population
- **Skills Matching** represents the degree of successful utilisation of skills, the extent to which skills are effectively matched in the labour market


#HSLIDE
### Statement
If we are serious about addressing the
skill gap, we need to simultaneously
address the recognition gap

#HSLIDE
### supporters: APPLAUSE!
If we are serious about addressing the
skill gap, we need to simultaneously
address the recognition gap

#HSLIDE
### opponents say: BOO!
If we are serious about addressing the
skill gap, we need to simultaneously
address the recognition gap

#HSLIDE
### Panel Opinion
If we are serious about addressing the
skill gap, we need to simultaneously
address the recognition gap

Note:

- Start (14u20) 5min
- End   (14u30) 15min

#HSLIDE

@snap[north-west personastatementpic]
![Issam](assets/images/Issam.png)
@snapend
@snap[north personastatementtop]
Issam tried to get his diploma recognised
@snapend


@snap[south personastatementbottom]
![dutch screen](assets/images/recogdiploma.png)
@snapend

Note:

Issam tried to get his diploma recognised but ended up on pages & forms which are only available in Dutch.



#HSLIDE

@snap[north-west personastatementpic]
![Issam](assets/images/Issam.png)
@snapend
@snap[west personastatementtop]
Someone pointed Issam to the Skills Profile Tool ...
@snapend


@snap[east personastatementright]
![Skills Profile Tool](assets/images/skillsprofiletool.png)
@snapend

#HSLIDE

@snap[north-west personastatementpic]
![Issam](assets/images/Issam.png)
@snapend
@snap[west personastatementtop]
... in which he can make claims about his skills with the help of ESCO
@snapend


@snap[east personastatementright]
![Esco](assets/images/esco.png)
@snapend

#HSLIDE

@snap[north-west personastatementpic]
![Sophie](assets/images/Sophie.png)
@snapend
@snap[north personastatementtop]
Sophie decided to make a profile on Linkedin
@snapend


@snap[south personastatementbottom]
![LinkedIn](assets/images/linkedin-logo.png)
@snapend


#HSLIDE
### Statement
We should promote the use of Linkedin more to people looking for a Job

#HSLIDE
### supporters: APPLAUSE!
We should promote the use of Linkedin more to people looking for a Job

#HSLIDE
### opponents say: BOO!
We should promote the use of Linkedin more to people looking for a Job

#HSLIDE
### Panel Opinion
We should promote the use of Linkedin more to people looking for a Job

Note:

- Start (14u33) 18min
- End   (14u40) 25min

#HSLIDE

@snap[north-west personastatementpic]
![Issam](assets/images/Issam.png)
@snapend
Issam, although very highly valued by his fellow students, is not having much success:
@ul[](false)
- his diploma didn't get recognised
- people don't give much value to his self-claimed skills
- only the people close to him trust him on his skills
@ulend

#HSLIDE

@snap[north-west personastatementpic]
![Sophie](assets/images/Sophie.png)
@snapend
A recruiter picked up the profile of Sophie on Linkedin 
@ul[](false)
- Sophie gets past 1st assessment
- just before interview she gets rejected based on not having the diploma yet
- she feels ready for the labor market and is very disappointed
@ulend


#HSLIDE
### Statement
It is a good thing that the labor market considers diploma's & certificates more important than the informal recognition & trust you get from your network.

#HSLIDE
### supporters: APPLAUSE!
It is a good thing that the labor market considers diploma's & certificates more important than the informal recognition & trust you get from your network.


#HSLIDE
### opponents say: BOO!
It is a good thing that the labor market considers diploma's & certificates more important than the informal recognition & trust you get from your network.

#HSLIDE
### Panel Opinion
It is a good thing that the labor market considers diploma's & certificates more important than the informal recognition & trust you get from your network.

Note:

- Start (14u43) 28min
- End   (14u53) 38min

#HSLIDE

@snap[north-west personastatementpic]
![Issam](assets/images/Issam.png)
@snapend
@snap[west personastatementtop]
In search of recognition Issam ended up at Apply Magic Sauce, a tool which builds an evidence based profile based on his digital footprints
@snapend


@snap[east personastatementright]
![ApplyMagicSauce](assets/images/applymagicsauce.png)
@snapend

#HSLIDE

@snap[north-west personastatementpic]
![Sophie](assets/images/Sophie.png)
@snapend
@snap[west personastatementtop]
Sophie is looking into getting micro-credentials as proof of skills she already has
@snapend

@snap[east personastatementright]
![OpenBadges](assets/images/bm_ob.png)
@snapend

#HSLIDE

@snap[north-west personastatementpic]
![Sophie](assets/images/Sophie.png)
@snapend
@snap[north-east personastatementpic]
![Issam](assets/images/Issam.png)
@snapend
Although Sophie & Issam are now carrying different verifiable records of their skills , 
it didn't solve their problem to be matched to and accepted in a job yet.


#HSLIDE
### Statement
If an organisation doesn't manage to recognize my skills, it will probably not be an organisation I want to work for.  

#HSLIDE
### supporters say: Yay!
If an organisation doesn't manage to recognize my skills, it will probably not be an organisation I want to work for.  

#HSLIDE
### opponents say: Nay!
If an organisation doesn't manage to recognize my skills, it will probably not be an organisation I want to work for.  

#HSLIDE
### Panel Opinion
If an organisation doesn't manage to recognize my skills, it will probably not be an organisation I want to work for.  


#HSLIDE

### Conclusion
What is probably the second most important skill to have ?

#HSLIDE

### Conclusion
The Skill to be able to get recognition. To be able to proof to others the skills you have! 


#HSLIDE

### Conclusion
So what is the most important one than ?

#HSLIDE

### Conclusion
The Skill to Recognise the Skills of others.

#HSLIDE

### Open Recognition Belgium
@snap[logoorb]
![ORB](assets/images/OpenRecBe_logo_600x600.png)
@snapend
####  http://openrecognition.be
#### @openrecbe

